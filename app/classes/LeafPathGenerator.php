<?php

namespace CategoryTree;

class LeafPathGenerator
{
    private $inputDir;
    private $outputDir;
    private $defaultTree;
    private $fileTree;

    public function __construct(string $inputPath, string $outputPath)
    {
        $this->inputDir = $inputPath;
        $this->outputDir = $outputPath;
        $this->defaultTree = [];
    }

    private function getChildren($key)
    {
        $children = [];
        $keys = array_keys($this->defaultTree);

        if (in_array($key, $keys)) {
            $children[$key] = [];
//            $children[$key] = $this->defaultTree[$key];

            foreach ($this->defaultTree[$key] as $word) {
                $children[$key][$word] = $word;
            }
        }

        if (count($children) > 0) {
            unset($this->defaultTree[$key]);
            return $children;
        }

        return false;
    }

    private function makeChildTree($child)
    {
        $childTree = [];

        if (isset($this->defaultTree[$child])) {
            foreach ($this->defaultTree[$child] as $item) {
                $children = $this->getChildren($item);
                $children = is_array($children) ? array_values($children) : false;
                $children = (is_array($children) && count($children) === 1) ? $children[0] : false;
                $childTree[$item] = $children !== false ? $children : $item;
            }

            unset($this->defaultTree[$child]);
        }

        return $childTree;
    }

    private function clearTree($tree)
    {
        $keys = array_keys($this->defaultTree);
        $newTree = [];

        foreach ($tree as $key => $children) {
            $newTree[$key] = [];

            foreach ($children as $index => $child) {
                if (in_array($child, $keys)) {
                    $childTree = $this->makeChildTree($child);

                    if (count($childTree)) {
                        $newTree[$key][$child] = $childTree;
                    }
                }
                else {
                    $strNewTree = json_encode($newTree);
                    $strOldTree = json_encode($this->defaultTree);
                    $foundInNew = strpos($strNewTree, $child);
                    $foundInOld = strpos($strOldTree, $child);

                    if ($foundInNew === false && $foundInOld !== false) {
                        $newTree[$key][$child] = $child;
                    }
                }

                unset($this->defaultTree[$key][$child]);
            }

            unset($this->defaultTree[$key]);
        }

        foreach ($newTree as $key => $values) {
            if (count($values) === 0) {
                unset($newTree[$key]);
            }
        }

//        echo "\n------------------------------\n";
//        var_dump($this->defaultTree);
//        var_dump($newTree);
//        exit;

        return $newTree;
    }

    private function makeFileTree($path)
    {
        $defaultTree = [];
        $lines = file($path);

        foreach ($lines as $line) {
            $words = explode(' ', trim($line));

            if (!isset($defaultTree[$words[0]])) {
                $defaultTree[$words[0]] = [];
            }

            $defaultTree[$words[0]][] = $words[1];
        }

        $this->defaultTree = $defaultTree;
        $tree = $this->clearTree($defaultTree);

        return $tree;
    }

    private function getLeafs($node, &$results)
    {
        if (is_array($node)) {
            foreach ($node as $key => $child) {
                $this->getLeafs($child, $results);
            }
        }
        else {
            $results[] = $node;
        }
    }

    private function getKeyParents($subject, $array)
    {
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                if (in_array($subject, array_keys($value)))
                    return array($key);
                else
                {
                    $chain = $this->getKeyParents($subject, $value);
                    if (!is_null($chain))
                        return array_merge(array($key), $chain);
                }
            }
        }

        return null;
    }

    private function processFile($file)
    {
        $path = $this->inputDir . $file;
        $routes = [];
        $leafs = [];
        $tree = $this->makeFileTree($path);
        $this->getLeafs($tree, $leafs);
        array_multisort($leafs, SORT_ASC);

        foreach ($leafs as $leaf) {
            $parentRoutes = $this->getKeyParents($leaf, $tree);
            $parentRoutes = array_reverse($parentRoutes);
            $routes[] = $leaf . ' ' . implode(' ', $parentRoutes);
        }

        $fileContent = implode("\n", $routes);
        $outputPath = $this->outputDir . $file;
        file_put_contents($outputPath, $fileContent);
    }

    public function start()
    {
        $files = scandir($this->inputDir);
        unset($files[array_search('.', $files, true)]);
        unset($files[array_search('..', $files, true)]);

        foreach ($files as $file) {
            $this->processFile($file);
        }
    }
}
