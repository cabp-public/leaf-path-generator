<?php

//$array = ['a', 'b', 'c', 'd', 'e'];
//array_splice($array, 1, 1);
//var_dump($array);
//exit;

require __DIR__ . '/../vendor/autoload.php';

$ds = DIRECTORY_SEPARATOR;
$dataDir = dirname(__DIR__) . $ds . '_data' . $ds;
$pathInput = $dataDir . 'input' . $ds;
$pathOutput = $dataDir . 'output' . $ds;
//$pathOutput = dirname(dirname(__DIR__)) . $ds . '02_ParserOutput' . $ds;

$leafPathGen = new \CategoryTree\LeafPathGenerator($pathInput, $pathOutput);
$leafPathGen->start();
